import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

import {  AuthGuard } from './Services/route-guard.service';

import { AppComponent } from './app.component';
import { DashboardComponent } from './Views/Dashboard/dashboard.component';
import { BasketComponent } from './Views/Dashboard/Content/basket.component';
import { ForumComponent } from './Views/Dashboard/Content/forum.component';
import { JobSearchComponent } from './Views/Dashboard/Content/job-search.component';
import { ToolboxComponent } from './Views/Dashboard/Content/toolbox.component';
import { UpdateComponent } from './Views/Dashboard/Content/update.component';
import { UserComponent } from './Views/Dashboard/Content/user.component';
import { TrainerComponent } from './Views/Dashboard/Content/trainer.component';

import { SignInComponent } from './Views/Account/sign-in.component';
import { SignUpComponent } from './Views/Account/sign-up.component';


import { ErrorComponent } from './Views/error.component';


const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  { path: 'sign-in', component: SignInComponent, }, /*canActivate: [EmptyLinkGuard]*/
  { path: 'sign-up', component: SignUpComponent },


 /* { path: 'user/:id', component: UserPageComponent },*/
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'update',
        pathMatch: 'full'
      },
      {
        path: 'update',
        component: UpdateComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'job-search',
        component: JobSearchComponent
      },
      {
        path: 'basket',
        component: BasketComponent
      },
      {
        path: 'toolbox',
        component: ToolboxComponent
      },
      {
        path: 'forum',
        component: ForumComponent
      },
      {
        path: 'trainer',
        component: TrainerComponent
      }
    ]
    /*  {
        path: 'user/edit/:id',
        component: AddTestRecordComponent
      },
      {
        path: 'records/edit/:id',
        component: AddTestRecordComponent
      }*/

  },

//  { path: 'detail/:id', component: ItemDetailComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false, initialNavigation: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
//, { enableTracing: true, useHash: true, initialNavigation: true }
