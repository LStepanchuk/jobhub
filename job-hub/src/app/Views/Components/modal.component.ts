﻿import { Component, ElementRef, Input, Output, OnInit, OnDestroy, EventEmitter, HostListener } from '@angular/core';
//import * as $ from 'jquery';

import { ModalService } from '../../Services/modal.service';

@Component({
    // moduleId: module.id.toString(),
    selector: 'modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css']
})

export class ModalComponent {//implements OnInit, OnDestroy
    @Input() id: string;
    @Output() dismiss = new EventEmitter();
    @Output() ok = new EventEmitter();
//    private element: JQuery;
 /*   private isDismissed: boolean = true;

    constructor(private modalService: ModalService, private el: ElementRef) {
        this.element = $(el.nativeElement);
    }

    @HostListener('window:keydown', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (!this.isDismissed) {
            switch (event.keyCode) {
                case 9:
                    event.preventDefault();
                    break;
                case 13:
                    event.preventDefault();
                    this.ok.emit();
                    break;
            }
        }
    }

    ngOnInit(): void {
        let modal = this;

        // ensure id attribute exists
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        // move element to bottom of page (just before </body>) so it can be displayed above everything else
        this.element.appendTo('body');

        // close modal on background click
        //this.element.on('click', function (e: any) {
        //    var target = $(e.target);
        //    if (!target.closest('.modal-body').length) {
        //        modal.close();
        //    }
        //});

        // add self (this modal instance) to the modal service so it's accessible from controllers
        this.modalService.add(this);
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    // open modal
    open(): void {
        this.isDismissed = false;
        this.element.show();
        this.element.focus();
        $('body').addClass('modal-open');
    }

    dismissModal(): void {
        if (!this.isDismissed) {
            this.dismiss.emit();
            this.close();
        }
    }

    // close modal
    close(): void {
        this.isDismissed = true;
        this.element.hide();
        $('body').removeClass('modal-open');
    }*/
}
