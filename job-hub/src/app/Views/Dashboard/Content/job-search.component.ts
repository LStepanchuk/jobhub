﻿import { Component } from '@angular/core';

@Component({
    selector: 'job-search',
    templateUrl: './job-search.component.html',
    styleUrls: ['./job-search.component.css']
})
export class JobSearchComponent {}
