﻿export class UserViewModel {
    FirstName: string;
    LastName: string;
    Email: string;
    Phone: string;
    PasswordHash: string;
    EmailConfirmed: boolean;
    PhoneConfirmed: boolean;
}

export class SignUpViewModel {
    FirstName: string;
    LastName: string;
    Email: string;
    Phone: string;
    Password: string;
    ConfirmPassword: string;
}

export class SignInViewModel {
    Username: string;
    Password: string;
    RememberMe: boolean;
}

export class RoleViewModel {
    Id: number;
    Name: string;
}
