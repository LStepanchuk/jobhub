﻿import { Component, OnInit } from '@angular/core';
import { Alert, AlertType } from '../../Models/alert';
import { AlertService } from '../../Services/alert.service';

@Component({
 //   moduleId: module.id,
    selector: 'alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})

     
export class AlertComponent {
    alerts: Alert[] = [];

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alert: Alert) => {
            if (!alert) {
                // clear alerts when an empty alert is received
                this.alerts = [];
                return;
            }

            // add alert to array
           // this.alerts = [];

            this.alerts.push(alert);
        });
    }

    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    img(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'ic_message_success.svg';
            case AlertType.Error:
                return 'ic_message_failed.svg';
            case AlertType.Info:
                return 'ic_message_info.svg';
            case AlertType.Warning:
                return 'ic_message_failed.svg';
        }
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                //return 'ui blue basic label';
                return 'alert-success';
            case AlertType.Error:
                //return 'ui red basic label';
                return 'alert-error';
            case AlertType.Info:
                //return 'ui orange basic label';
                return 'alert-info'
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}