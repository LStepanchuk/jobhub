import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { DashboardComponent } from './Views/Dashboard/dashboard.component';
import { BasketComponent } from './Views/Dashboard/Content/basket.component';
import { ForumComponent } from './Views/Dashboard/Content/forum.component';
import { JobSearchComponent } from './Views/Dashboard/Content/job-search.component';
import { ToolboxComponent } from './Views/Dashboard/Content/toolbox.component';
import { UpdateComponent } from './Views/Dashboard/Content/update.component';
import { UserComponent } from './Views/Dashboard/Content/user.component';
import { TrainerComponent } from './Views/Dashboard/Content/trainer.component';


import { SignInComponent } from './Views/Account/sign-in.component';
import { SignUpComponent } from './Views/Account/sign-up.component';

import { AlertComponent } from './Views/Components/alert.component';
import { ModalComponent } from './Views/Components/modal.component';
import { MainMenuComponent } from './Views/Components/main-menu.component';
import { ErrorComponent } from './Views/error.component';

import { AppRoutingModule } from './app.routing.module';
import { AlertService } from './Services/alert.service';
import { ModalService } from './Services/modal.service';
import { AuthGuard } from './Services/route-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    SignInComponent,
    SignUpComponent,
    DashboardComponent,
    BasketComponent,
    ForumComponent,
    JobSearchComponent,
    ToolboxComponent,
    TrainerComponent,
    UpdateComponent,
    UserComponent,
    ErrorComponent,
    ModalComponent,
    MainMenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    AlertService,
    ModalService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
